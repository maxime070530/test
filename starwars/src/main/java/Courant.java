public class Courant extends Compte{
    @Override
    public String toString() {
        return "CompteCourant{" +
                "titulaire='" + super.getTitulaire() + '\'' +
                ", solde=" + super.getSolde() +
                ", numero=" + super.getNumero() +
                ", titre='" + super.getTitre() + '\'' +
                '}';
    }
}
