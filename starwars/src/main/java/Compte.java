public abstract class Compte {
    private String titulaire;
    private double solde;
    private int numero;
    private String titre;

    public double getSolde() {
        return solde;
    }

    public void crediter(int montant){
        solde = solde + montant;
    }

    public void debiter(int montant){
        solde = solde - montant;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "titulaire='" + titulaire + '\'' +
                ", solde=" + solde +
                ", numero=" + numero +
                ", titre='" + titre + '\'' +
                '}';
    }
}
