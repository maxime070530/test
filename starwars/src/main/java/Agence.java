import java.util.ArrayList;
import java.util.List;

public class Agence {
    private String nom;
    private List<Compte> listeComptes = new ArrayList<>();

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Compte> getListeComptes() {
        return listeComptes;
    }

    public void setListeComptes(List<Compte> listeComptes) {
        this.listeComptes = listeComptes;
    }

    public void ajouter(Compte compte){
        listeComptes.add(compte);
    }

    public void supprimer(int numero){
        for(Compte compte: listeComptes){
            if(compte.getNumero() == numero){
                listeComptes.remove(compte);
            }
        }
    }
}
