public class RemunereSecurise extends Remunere{
    public void debiter(int montant) {
        if (super.getSolde() > 0) {
            super.setSolde(super.getSolde() - montant);
        }
    }
}
