import java.util.ArrayList;

public class main {
    public static void main (String[] args){
        Agence agence = new Agence();
        agence.setNom("BNP CALVISSON");

        Compte compte = new Courant();
        compte.setTitulaire("Maxime Grau");
        compte.setSolde(600);
        compte.setNumero(1);
        compte.setTitre("Titre");

        Compte compte2 = new Courant();
        compte2.setTitulaire("Luca Altea");
        compte2.setSolde(600);
        compte2.setNumero(2);
        compte2.setTitre("Titre2");

        agence.ajouter(compte);
        agence.ajouter(compte2);

        System.out.println(agence.getListeComptes());
    }
}
